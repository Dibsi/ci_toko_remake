-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 07, 2019 at 08:59 AM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 7.0.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `toko_jaya_abadi`
--
CREATE DATABASE IF NOT EXISTS `toko_jaya_abadi` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `toko_jaya_abadi`;

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `kode_barang` varchar(5) NOT NULL,
  `nama_barang` varchar(150) NOT NULL,
  `harga_barang` float NOT NULL,
  `kode_jenis` varchar(5) NOT NULL,
  `flag` int(11) NOT NULL,
  `stok` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`kode_barang`, `nama_barang`, `harga_barang`, `kode_jenis`, `flag`, `stok`) VALUES
('BR001', 'Ayam', 10000, 'JN006', 1, 503),
('BR002', 'Spidol', 10000, 'JN001', 1, 200),
('BR003', 'Pensil', 10000, 'JN001', 1, 100),
('BR004', 'Solder', 150000, 'JN004', 1, 0),
('BR005', 'Jam Tangan', 150000, 'JN005', 1, 6);

-- --------------------------------------------------------

--
-- Table structure for table `jabatan`
--

CREATE TABLE `jabatan` (
  `kode_jabatan` varchar(5) NOT NULL,
  `nama_jabatan` varchar(100) NOT NULL,
  `keterangan` text NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jabatan`
--

INSERT INTO `jabatan` (`kode_jabatan`, `nama_jabatan`, `keterangan`, `flag`) VALUES
('JB001', 'Kasir', 'Operational', 1),
('JB002', 'Admin', 'Operational', 1),
('JB003', 'Karyawan', 'Operational', 1),
('JB004', 'Direktur', 'Keuangan', 1),
('JB005', 'Office Boy', 'Pembersih 1', 1),
('JB006', 'Office Girl', 'Pembersih', 1);

-- --------------------------------------------------------

--
-- Table structure for table `jenis_barang`
--

CREATE TABLE `jenis_barang` (
  `kode_jenis` varchar(5) NOT NULL,
  `nama_jenis` varchar(150) NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis_barang`
--

INSERT INTO `jenis_barang` (`kode_jenis`, `nama_jenis`, `flag`) VALUES
('JN001', 'Alat Tulis', 1),
('JN002', 'Perangkat Keras', 1),
('JN003', 'Alat Rumah Tangga', 1),
('JN004', 'Alat Listrik', 1),
('JN005', 'Mainan', 1),
('JN006', 'Makanan', 1);

-- --------------------------------------------------------

--
-- Table structure for table `karyawan`
--

CREATE TABLE `karyawan` (
  `nik` varchar(10) NOT NULL,
  `nama_lengkap` varchar(150) NOT NULL,
  `tempat_lahir` varchar(100) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `jenis_kelamin` varchar(1) NOT NULL,
  `alamat` text NOT NULL,
  `telp` varchar(15) NOT NULL,
  `kode_jabatan` varchar(5) NOT NULL,
  `photo` varchar(150) NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `karyawan`
--

INSERT INTO `karyawan` (`nik`, `nama_lengkap`, `tempat_lahir`, `tgl_lahir`, `jenis_kelamin`, `alamat`, `telp`, `kode_jabatan`, `photo`, `flag`) VALUES
('1902001', 'Adib Bisyri Musyaffa', 'Jakarta', '1998-12-11', 'L', 'Jl.Kalibaru Timur VC', '089621650023', 'JB001', '', 1),
('1902002', 'Achmad Chaidir', 'Bekasi', '1998-04-11', 'L', 'Jl.Bekasi Utara', '089658788545', 'JB002', '', 1),
('1902003', 'Manca Ilyasa Yahya', 'Karawang', '1998-08-04', 'L', 'Jl. Pocinki No.35', '086558784568', 'JB003', '', 1),
('1902004', 'Asta Black Clover', 'Cepu', '1976-04-03', 'L', 'Jl.Warakas 15', '086558784568', 'JB001', '20190402-1902004.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pembelian_detail`
--

CREATE TABLE `pembelian_detail` (
  `id_pembelian_d` int(11) NOT NULL,
  `id_pembelian_h` int(11) NOT NULL,
  `kode_barang` varchar(5) NOT NULL,
  `qty` int(11) NOT NULL,
  `harga` float NOT NULL,
  `jumlah` float NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembelian_detail`
--

INSERT INTO `pembelian_detail` (`id_pembelian_d`, `id_pembelian_h`, `kode_barang`, `qty`, `harga`, `jumlah`, `flag`) VALUES
(1, 12, 'BR001', 100, 600, 60000, 1),
(2, 12, 'BR001', 100, 600, 60000, 1),
(3, 12, 'BR001', 100, 600, 60000, 1),
(4, 12, 'BR001', 100, 600, 60000, 1),
(5, 12, 'BR001', 100, 600, 60000, 1),
(6, 13, 'BR002', 100, 600, 60000, 1),
(7, 13, 'BR001', 100, 600, 60000, 1),
(8, 13, 'BR001', 100, 600, 60000, 1),
(9, 13, 'BR003', 100, 85000, 8500000, 1),
(10, 14, 'BR005', 3, 600, 1800, 1),
(11, 14, 'BR005', 3, 600, 1800, 1),
(12, 14, 'BR002', 100, 600, 60000, 1),
(13, 15, 'BR001', 100, 600, 60000, 1),
(14, 16, 'BR001', 3, 2000, 6000, 1);

-- --------------------------------------------------------

--
-- Table structure for table `pembelian_header`
--

CREATE TABLE `pembelian_header` (
  `id_pembelian_h` int(11) NOT NULL,
  `no_transaksi` varchar(10) NOT NULL,
  `tgl` date NOT NULL,
  `kode_supplier` varchar(5) NOT NULL,
  `approved` int(11) NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembelian_header`
--

INSERT INTO `pembelian_header` (`id_pembelian_h`, `no_transaksi`, `tgl`, `kode_supplier`, `approved`, `flag`) VALUES
(1, 'TR001', '2019-03-26', 'SP002', 1, 1),
(2, 'TR002', '2019-03-26', 'SP001', 1, 1),
(3, 'TR001', '2019-03-26', 'SP001', 1, 1),
(4, 'TR002', '2019-03-26', 'SP001', 1, 1),
(5, 'TR004', '2019-03-26', 'SP001', 1, 1),
(6, 'TR004', '2019-03-26', 'SP001', 1, 1),
(7, 'TR005', '2019-03-26', 'SP001', 1, 1),
(8, 'TR006', '2019-03-26', 'SP001', 1, 1),
(9, 'TR007', '2019-03-26', 'SP001', 1, 1),
(10, 'TR008', '2019-03-26', 'SP001', 1, 1),
(11, 'TR001', '2019-04-06', 'SP001', 1, 1),
(12, 'TR002', '2019-04-06', 'SP001', 1, 1),
(13, 'TR004', '2019-04-06', 'SP001', 1, 1),
(14, 'TR005', '2019-04-07', 'SP002', 1, 1),
(15, 'TR006', '2019-04-07', 'SP004', 1, 1),
(16, 'TR007', '2019-04-07', 'SP003', 1, 1),
(17, '', '2019-04-07', '', 1, 1),
(18, '', '2019-04-07', '', 1, 1),
(19, '', '2019-04-07', '', 1, 1),
(20, '', '2019-04-07', '', 1, 1),
(21, '', '2019-04-07', '', 1, 1),
(22, '', '2019-04-07', '', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `penjualan_detail`
--

CREATE TABLE `penjualan_detail` (
  `id_jual_d` int(11) NOT NULL,
  `id_jual_h` int(11) NOT NULL,
  `kode_barang` varchar(5) NOT NULL,
  `qty` int(11) NOT NULL,
  `harga` float NOT NULL,
  `jumlah` float NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `penjualan_header`
--

CREATE TABLE `penjualan_header` (
  `id_jual_h` int(11) NOT NULL,
  `no_transaksi` varchar(10) NOT NULL,
  `tanggal` date NOT NULL,
  `approved` int(11) NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `kode_supplier` varchar(5) NOT NULL,
  `nama_supplier` varchar(150) NOT NULL,
  `alamat` text NOT NULL,
  `telp` varchar(15) NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`kode_supplier`, `nama_supplier`, `alamat`, `telp`, `flag`) VALUES
('SP001', 'CV. Informa Abadi', 'Jl. Mangga Dua Raya No.15', '0213383838', 1),
('SP002', 'CV. Super Baja', 'Jl. Cilincing Raya No.10', '0214415677', 1),
('SP003', 'CV. Maju Sekali', 'Jl.Warakas Raya No 16', '082297589632', 1),
('SP004', 'CV. Sakti Sekali', 'Jl. Cacing Raya No 17', '0214417655', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `nik` varchar(10) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(32) NOT NULL,
  `tipe` int(11) NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `nik`, `email`, `password`, `tipe`, `flag`) VALUES
(1, '1704421300', 'adibsee18@gmail.com', '182696da0efe054b1126e3038175a097', 2, 1),
(2, 'admin', 'admintoko@gmail.com', '0192023a7bbd73250516f069df18b500', 1, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`kode_barang`);

--
-- Indexes for table `jabatan`
--
ALTER TABLE `jabatan`
  ADD PRIMARY KEY (`kode_jabatan`);

--
-- Indexes for table `karyawan`
--
ALTER TABLE `karyawan`
  ADD PRIMARY KEY (`nik`);

--
-- Indexes for table `pembelian_detail`
--
ALTER TABLE `pembelian_detail`
  ADD PRIMARY KEY (`id_pembelian_d`);

--
-- Indexes for table `pembelian_header`
--
ALTER TABLE `pembelian_header`
  ADD PRIMARY KEY (`id_pembelian_h`);

--
-- Indexes for table `penjualan_detail`
--
ALTER TABLE `penjualan_detail`
  ADD PRIMARY KEY (`id_jual_d`);

--
-- Indexes for table `penjualan_header`
--
ALTER TABLE `penjualan_header`
  ADD PRIMARY KEY (`id_jual_h`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`kode_supplier`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pembelian_detail`
--
ALTER TABLE `pembelian_detail`
  MODIFY `id_pembelian_d` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `pembelian_header`
--
ALTER TABLE `pembelian_header`
  MODIFY `id_pembelian_h` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `penjualan_detail`
--
ALTER TABLE `penjualan_detail`
  MODIFY `id_jual_d` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `penjualan_header`
--
ALTER TABLE `penjualan_header`
  MODIFY `id_jual_h` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
