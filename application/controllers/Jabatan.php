<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Jabatan extends CI_Controller{
	
	public function __construct()
{
	parent:: __construct();
	$user_login	=$this->session->userdata();

	if(count($user_login)<=1) {
		redirect("user", "refresh");

	}
	$this->load->model("jabatan_model");
}

	public function index()
{
	$this->ListJabatan();

}

	public function ListJabatan()
{
	if (isset($_POST['tombol_cari'])) {
		$data['kata_pencarian'] = $this->input->post('caridata');
		$this->session->set_userdata('session_pencarian_jabatan', $data['kata_pencarian']);
	} else {
		$data['kata_pencarian'] = $this->session->userdata('session_pencarian_jabatan');
	}
	//$data['data_karyawan']= $this->karyawan_model->tampilDataKaryawan();
	$data['data_jabatan']= $this->jabatan_model->tombolpagination($data
	['kata_pencarian']);
	//$data['data_jabatan']= $this->jabatan_model->tampilDataJabatan();
	$data['content']='form/list_jabatan';
	$this->load->view('home-2',$data);
}
	public function InputJabatan()
{
	$data['data_jabatan']= $this->jabatan_model->CreateKodeUrut();
	$data['content'] = 'input/input_jabatan';
	//if (!empty($_REQUEST)) {
		//$m_jabatan = $this->jabatan_model;
		//$m_jabatan->save();
		//redirect("jabatan/index", "refresh");
	$validation = $this->form_validation;
	$validation->set_rules($this->jabatan_model->rules());
	if ($validation->run()){
		$this->jabatan_model->save();
		redirect("jabatan/index", "refresh");
	}
	$this->load->view('home-2',$data);
 }
 	public function detailjabatan($kode_jabatan)
	{
		$data['detail_jabatan'] = $this->jabatan_model->detail($kode_jabatan);
		$data['content'] = 'detail/detail_jabatan';
		$this->load->view('home-2', $data);
	}
	public function EditJabatan($kode_jabatan)
{
	$data['detail_jabatan'] = $this->jabatan_model->detail($kode_jabatan);
	$data['content'] = 'edit/edit_jabatan';
	//if (!empty($_REQUEST)) {
		//$m_jabatan = $this->jabatan_model;
		//$m_jabatan->update($kode_jabatan);
		//redirect("jabatan/index", "refresh");
	$validation = $this->form_validation;
	$validation->set_rules($this->jabatan_model->rules());
	if ($validation->run()){
		$this->jabatan_model->update($kode_jabatan);
		redirect("jabatan/index", "refresh");
	}
	$this->load->view('home-2',$data);
	}
	public function deletejabatan($kode_jabatan)
 	{
 		$m_jabatan = $this->jabatan_model;
 		$m_jabatan->delete($kode_jabatan);
 		redirect("jabatan/index", "refresh");
 	}
}



