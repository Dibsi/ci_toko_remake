<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Penjualan extends CI_Controller{
	
	public function __construct()
{
	parent:: __construct();
	$this->load->model("Penjualan_model");
	$this->load->model("Supplier_model");
	$this->load->model("Barang_model");
	$this->load->library('pdf');
}
	public function index()
{
	$this->ListPenjualan();
}

public function ListPenjualan1()
{
	if (isset($_POST['tombol_cari'])) {
		$data['kata_pencarian'] = $this->input->post('caridata');
		$this->session->set_userdata('session_pencarian_penjualan', $data['kata_pencarian']);
	} else {
		$data['kata_pencarian'] = $this->session->userdata('session_pencarian_penjualan');
	}
	//$data['data_karyawan']= $this->karyawan_model->tampilDataKaryawan();
	$data['data_penjualan']= $this->Penjualan_model->tombolpagination($data
	['kata_pencarian']);
	//$data['data_jabatan']= $this->jabatan_model->tampilDataJabatan();
	$data['content']='form/list_penjualan1';
	$this->load->view('home-2',$data);
}

public function InputPenjualan()
{
	$data['data_penjualan']= $this->Penjualan_model->notransaksi();
	$data['data_supplier']= $this->Supplier_model->tampilDataSupplier();
	
	$data['content'] = 'input/input_penjualan';
	//if (!empty($_REQUEST)) {
		//$pembelian_header->savePembelianHeader();
		
		
		$validation = $this->form_validation;
		$validation->set_rules($this->Penjualan_model->rules());
		if ($validation->run()){
		$penjualan_header = $this->Penjualan_model;
		$this->Penjualan_model->savePenjualanHeader();
		$id_terakhir = $penjualan_header->idTransaksiTerakhir();
		redirect("penjualan/InputPenjualanDetail/" . $id_terakhir, "refresh");
		 }
	$this->load->view('home-2',$data);
	}

	public function InputPenjualan1()
	{
		$data['content'] = 'penjualan';
		$this->load->view('home-2',$data);
	}

	public function ListPenjualan()
{
		$tgl_awal=$this->input->post('tgl_awal');
		$pisah=explode('/', $tgl_awal);
		$array=array($pisah[2],$pisah[0],$pisah[1]);
		$tgl_awal=implode('-', $array);

		$tgl_akhir=$this->input->post('tgl_akhir');
		$pisah=explode('/', $tgl_akhir);
		$array=array($pisah[2],$pisah[0],$pisah[1]);
		$tgl_akhir=implode('-', $array);


	$data['tgl_awal']=$this->input->post('tgl_awal');
	$data['tgl_akhir']=$this->input->post('tgl_akhir');
	$data['data_penjualan_detail']= $this->Penjualan_model->tampilBaru($tgl_awal,$tgl_akhir);
	$data['content'] = 'form/list_penjualan';

$this->load->view('home-2',$data);
	}

	public function InputPenjualanDetail($id_penjualan_header)
{
	$data['data_barang']= $this->Barang_model->tampilDataBarang2();
	$data['id_header']= $id_penjualan_header;
	$data['data_penjualan_detail']= $this->Penjualan_model->tampilDataPenjualanDetail($id_penjualan_header);
	$data['content'] = 'input/input_penjualan_d';
	//if (!empty($_REQUEST)) {
		$validation = $this->form_validation;
		$validation->set_rules($this->Penjualan_model->rules1());

		if ($validation->run()){
		$this->Penjualan_model->savePenjualanDetail($id_penjualan_header);
		$kode_barang = $this->input->post('kode_barang');
		$qty = $this->input->post('qty');
		$this->Barang_model->updateStok1($kode_barang, $qty);
		
		redirect("penjualan/InputPenjualandetail/" . $id_penjualan_header, "refresh");
		 }
	$this->load->view('home-2',$data);
	}

	function CetakPdf($tgl_awal,$tgl_akhir)
	{
		ob_start();
        $pdf = new FPDF('l','mm','A5');
        // membuat halaman baru
        $pdf->AddPage();
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial','B',16);
        // mencetak string 
        $pdf->Cell(200,7,'Report Penjualan',0,1,'C');
        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(200,7,'Toko Jaya Abadi',0,1,'C');
        $pdf->SetFont('Arial','B',11);
        $pdf->Cell(200,7,$tgl_awal." s/d ".$tgl_akhir,0,1,'C');
        // Memberikan space kebawah agar tidak terlalu rapat
        $pdf->Cell(10,8,'',0,1);
        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(27,6,'Id Penjualan H',1,0,'C');
        $pdf->Cell(40,6,'No Transaksi',1,0,'C');
        $pdf->Cell(30,6,'Tgl Penjualan',1,0,'C');
        $pdf->Cell(30,6,'Total Barang',1,0,'C');
        $pdf->Cell(25,6,'Qty',1,0,'C');
        $pdf->Cell(40,6,'Jumlah Penjualan',1,1,'C');
    
        $pdf->SetFont('Arial','',10);
        $data_penjualan_detail= $this->Penjualan_model->HasilPdf($tgl_awal,$tgl_akhir);

        $total_hitung = 0;
        foreach ($data_penjualan_detail as $data){
          	$jumlah = "Rp " . number_format($data->jumlah,2,',','.');
            $pdf->Cell(27,6,$data->id_penjualan_h,1,0,'C');
            $pdf->Cell(40,6,$data->no_transaksi,1,0,'C');
            $pdf->Cell(30,6,$data->tgl,1,0,'C');
            $pdf->Cell(30,6,$data->stok,1,0,'C');
            $pdf->Cell(25,6,$data->qty,1,0,'C');
            $pdf->Cell(40,6,"Rp.".number_format($data->jumlah),1,1,'R');
            $total_hitung += $data->jumlah;
     
    }
     $pdf->SetFont('Arial','B',10);
        $pdf->Cell(152,6,'Total Keseluruhan',1,0,'C');
        $pdf->Cell(40,6,"Rp.".number_format($total_hitung),1,1,'R');
        $pdf->Output();

}
}