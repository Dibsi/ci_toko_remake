<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Karyawan extends CI_Controller{
	
	public function __construct()
{
	parent:: __construct();
	$user_login	=$this->session->userdata();

	if(count($user_login)<=1) {
		redirect("user", "refresh");

	}
	$this->load->model("karyawan_model");
	$this->load->model("jabatan_model");
}

	public function index()
{
	$this->ListKaryawan();


}

	public function ListKaryawan()
{
	if (isset($_POST['tombol_cari'])) {
		$data['kata_pencarian'] = $this->input->post('caridata');
		$this->session->set_userdata('session_pencarian_karyawan', $data['kata_pencarian']);
	} else {
		$data['kata_pencarian'] = $this->session->userdata('session_pencarian_karyawan');
	}
	//$data['data_karyawan']= $this->karyawan_model->tampilDataKaryawan();
	$data['data_karyawan']= $this->karyawan_model->tombolpagination($data
	['kata_pencarian']);
	$data['content']='form/list_karyawan';
	$this->load->view('home-2', $data);
}
	public function InputKaryawan()
{
	$nik_karyawan = $this->input->post('nik');
	$data['data_jabatan']= $this->jabatan_model->tampilDataJabatan3();
	$data['data_karyawan']= $this->karyawan_model->KodeUrut();
	$data['content'] = 'input/input_karyawan';
	//if (!empty($_REQUEST)) {
		//$m_karyawan = $this->karyawan_model;
		//$m_karyawan->save();
		//redirect("karyawan/index", "refresh");
	$validation = $this->form_validation;
	$validation->set_rules($this->karyawan_model->rules());
	if ($validation->run()){
		$this->karyawan_model->save();
		redirect("karyawan/index", "refresh");
	}
	$this->load->view('home-2',$data);
 }
 	public function detailkaryawan($nik)
	{
		$data['detail_karyawan'] = $this->karyawan_model->detail($nik);
		$data['content'] = 'detail/detail_karyawan';
		$this->load->view('home-2', $data);
	}
	public function EditKaryawan($nik)
{
	$data['data_jabatan']= $this->jabatan_model->TampilDataJabatan3();
	$data['detail_karyawan'] = $this->karyawan_model->detail($nik);
	$data['content'] = 'edit/edit_karyawan';
	//if (!empty($_REQUEST)) {
		//$m_karyawan = $this->karyawan_model;
		//$m_karyawan->update($nik);
		//redirect("karyawan/index", "refresh");
	$validation = $this->form_validation;
	$validation->set_rules($this->karyawan_model->rules1());
	if ($validation->run()){
		$this->karyawan_model->update($nik);
		redirect("karyawan/index", "refresh");
}
	$this->load->view('home-2',$data);
 	}
 	public function deletekaryawan($nik)
 	{
 		$m_karyawan = $this->karyawan_model;
 		$m_karyawan->delete($nik);
 		redirect("karyawan/index", "refresh");
 	}
	}


