<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Barang extends CI_Controller{
	
	public function __construct()
{
	parent:: __construct();
	$user_login	=$this->session->userdata();

	if(count($user_login)<=1) {
		redirect("user", "refresh");

	}
	$this->load->model("barang_model");
	$this->load->model("jenis_model");

	$this->load->library('form_validation');
}

	public function index()
{
	$this->ListBarang();

}

	public function ListBarang()
{
		if (isset($_POST['tombol_cari'])) {
		$data['kata_pencarian'] = $this->input->post('caridata');
		$this->session->set_userdata('session_pencarian_barang', $data['kata_pencarian']);
	} else {
		$data['kata_pencarian'] = $this->session->userdata('session_pencarian_barang');
	}
	//$data['data_karyawan']= $this->karyawan_model->tampilDataKaryawan();
	$data['data_barang']= $this->barang_model->tombolpagination($data
	['kata_pencarian']);
	//$data['data_barang']= $this->barang_model->tampilDataBarang2();
	$data['content']='form/list_barang';
	$this->load->view('home-2',$data);
}
	public function InputBarang()
{
	$data['data_jenis']= $this->jenis_model->tampilDataJenis3();
	$data['data_barang']= $this->barang_model->CreateKodeUrut();
	$data['content'] = 'input/input_barang';
	//if (!empty($_REQUEST)) {
		//$m_barang = $this->barang_model;
		//$m_barang->save();
		//redirect("barang/index", "refresh");
	//}
	$validation = $this->form_validation;
	$validation->set_rules($this->barang_model->rules());
	if ($validation->run()){
		$this->barang_model->save();
		redirect("barang/index", "refresh");
	}
	$this->load->view('home-2',$data);
 }
 	public function detailbarang($kode_barang)
	{
		$data['detail_barang'] = $this->barang_model->detail($kode_barang);
		$data['content'] = 'detail/detail_barang';
		$this->load->view('home-2', $data);
	}
	public function EditBarang($kode_barang)
{
	$data['data_jenis']= $this->jenis_model->tampilDataJenis3();
	$data['detail_barang'] = $this->barang_model->detail($kode_barang);
	$data['content'] = 'edit/edit_barang';
	//if (!empty($_REQUEST)) {
		//$m_barang = $this->barang_model;
		//$m_barang->update($kode_barang);
		//redirect("barang/index", "refresh");
	$validation = $this->form_validation;
	$validation->set_rules($this->barang_model->rules());
	if ($validation->run()){
		$this->barang_model->update($kode_barang);
		redirect("barang/index", "refresh");
	}
	$this->load->view('home-2',$data);
 	}

 	public function deletebarang($kode_barang)
 	{
 		$m_barang = $this->barang_model;
 		$m_barang->delete($kode_barang);
 		redirect("barang/index", "refresh");
 	}



	}

