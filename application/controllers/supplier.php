<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Supplier extends CI_Controller{
	
	public function __construct()
{
	parent:: __construct();
	$user_login	=$this->session->userdata();

	if(count($user_login)<=1) {
		redirect("user", "refresh");

	}
	$this->load->model("supplier_model");
}

	public function index()
{
	$this->ListSupplier();

}

	public function ListSupplier()
{
	if (isset($_POST['tombol_cari'])) {
		$data['kata_pencarian'] = $this->input->post('caridata');
		$this->session->set_userdata('session_pencarian_supplier', $data['kata_pencarian']);
	} else {
		$data['kata_pencarian'] = $this->session->userdata('session_pencarian_supplier');
	}
	//$data['data_karyawan']= $this->karyawan_model->tampilDataKaryawan();
	$data['data_supplier']= $this->supplier_model->tombolpagination($data
	['kata_pencarian']);
	//$data['data_supplier']= $this->supplier_model->tampilDataSupplier();
	$data['content']='form/list_supplier';
	$this->load->view('home-2',$data);
}
	public function InputSupplier()
{
	$data['data_supplier']= $this->supplier_model->CreateKodeUrut();
	$data['content'] = 'input/input_supplier';
	//if (!empty($_REQUEST)) {
		//$m_supplier = $this->supplier_model;
		//$m_supplier->save();
		//redirect("supplier/index", "refresh");
	$validation = $this->form_validation;
	$validation->set_rules($this->supplier_model->rules());
	if ($validation->run()){
		$this->supplier_model->save();
		redirect("supplier/index", "refresh");
	}
	$this->load->view('home-2',$data);
 }
 	public function detailsupplier($kode_supplier)
	{
		$data['detail_supplier'] = $this->supplier_model->detail($kode_supplier);
		$data['content'] = 'detail/detail_supplier';
		$this->load->view('home-2', $data);
	}
	public function EditSupplier($kode_supplier)
{
	$data['detail_supplier'] = $this->supplier_model->detail($kode_supplier);
	$data['content'] = 'edit/edit_supplier';
	//if (!empty($_REQUEST)) {
		//$m_supplier = $this->supplier_model;
		//$m_supplier->update($kode_supplier);
		//redirect("supplier/index", "refresh");
	$validation = $this->form_validation;
	$validation->set_rules($this->supplier_model->rules());
	if ($validation->run()){
		$this->supplier_model->update($kode_supplier);
		redirect("supplier/index", "refresh");
}
	$this->load->view('home-2',$data);
 	}
 	public function deletesupplier($kode_supplier)
 	{
 		$m_supplier = $this->supplier_model;
 		$m_supplier->delete($kode_supplier);
 		redirect("supplier/index", "refresh");
 	}
	}


