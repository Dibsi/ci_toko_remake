<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Jenis extends CI_Controller{
	
	public function __construct()
{
	parent:: __construct();
	$user_login	=$this->session->userdata();

	if(count($user_login)<=1) {
		redirect("user", "refresh");

	}
	$this->load->model("jenis_model");
}

	public function index()
{
	$this->ListJenis();

}

	public function ListJenis()
{
	if (isset($_POST['tombol_cari'])) {
		$data['kata_pencarian'] = $this->input->post('caridata');
		$this->session->set_userdata('session_pencarian_jenis', $data['kata_pencarian']);
	} else {
		$data['kata_pencarian'] = $this->session->userdata('session_pencarian_jenis');
	}
	//$data['data_karyawan']= $this->karyawan_model->tampilDataKaryawan();
	$data['data_jenis']= $this->jenis_model->tombolpagination($data
	['kata_pencarian']);
	//$data['data_jenis']= $this->jenis_model->tampilDataJenis();
	$data['content']='form/list_jenis';
	$this->load->view('home-2',$data);
}
public function InputJenis()
{
	$data['data_jenis']= $this->jenis_model->CreateKodeUrut();
	$data['content'] = 'input/input_jenis';
	//if (!empty($_REQUEST)) {
		//$m_jenis = $this->jenis_model;
		//$m_jenis->save();
		//redirect("jenis/index", "refresh");
	$validation = $this->form_validation;
	$validation->set_rules($this->jenis_model->rules());
	if ($validation->run()){
		$this->jenis_model->save();
		redirect("jenis/index", "refresh");
	}
	$this->load->view('home-2',$data);
 }
 	public function detailjenis($kode_jenis)
	{
		$data['detail_jenis'] = $this->jenis_model->detail($kode_jenis);
		$data['content'] = 'detail/detail_jenis';
		$this->load->view('home-2', $data);
	}
	public function EditJenis($kode_jenis)
{
	$data['detail_jenis'] = $this->jenis_model->detail($kode_jenis);
	$data['content'] = 'edit/edit_jenis';
	//if (!empty($_REQUEST)) {
		//$m_jenis = $this->jenis_model;
		//$m_jenis->update($kode_jenis);
		//redirect("jenis/index", "refresh");
	$validation = $this->form_validation;
	$validation->set_rules($this->jenis_model->rules());
	if ($validation->run()){
		$this->jenis_model->update($kode_jenis);
		redirect("jenis/index", "refresh");
}
	$this->load->view('home-2',$data);
 	}
 	public function deletejenis($kode_jenis)
 	{
 		$m_jenis = $this->jenis_model;
 		$m_jenis->delete($kode_jenis);
 		redirect("jenis/index", "refresh");
 	}
	}


