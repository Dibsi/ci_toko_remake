<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penjualan_model extends CI_Model{
	private $_table_header = "penjualan_header";
	private $_table_detail = "penjualan_detail";

	public function rules()
	{
		return[
		[
			'field' 		=> 'no_transaksi',
			'label' 		=> 'No Transaksi',
			'rules' 		=> 'required[max_length[5]',
			'errors'		=>[
			'required' 		=> 'No Transaksi Tidak Boleh Kosong.',
			'max_length'	=> 'No Transaksi Tidak Boleh lebih dari 5 karakter.',
			],
		],
		[
			'field' 		=> 'nama_supplier',
			'label' 		=> 'Nama Pembeli',
			'rules' 		=> 'required',
			'errors'		=> [
			'required' 		=> 'Nama Pembeli Tidak Boleh Kosong.',
			],
		]
		];
	}

	public function rules1()
    
    {
        return [
        [
        'field' => 'kode_barang',
        'label' => 'kode barang',
        'rules' => 'required|max_length[5]',
        'errors' => [
           'required' => 'kode barang tidak boleh kosong.',
           'max_length' => 'kode barang tidak boleh lebih dari 5 karakter.',
           ]
           ],
           [
         'field' => 'qty',
         'label' => 'qty',
         'rules' => 'required|numeric',
         'errors' => [
           'required' => 'qty tidak boleh kosong.',
           'numeric' => 'qty harus angka.',
            ]
           ],
           ];
    }

    public function TampilDataPenjualanPagination($perpage, $uri, $data_pencarian)
	{
	$this->db->select('*');
	if (!empty($data_pencarian)) {
		$this->db->like('no_transaksi', $data_pencarian);
	}
	$this->db->order_by('id_penjualan_h','ASC');

	$get_data = $this->db->get($this->_table_header, $perpage, $uri);
	if ($get_data->num_rows() > 0) {
		return $get_data->result();
	} else {
		return null;
		}
	}
	public function tombolpagination($data_pencarian)
	{
	$this->db->like('no_transaksi', $data_pencarian);
	$this->db->from($this->_table_header);
	$hasil = $this->db->count_all_results();

	$pagination['base_url'] 	= base_url().'penjualan/listpenjualan1/load/';
	$pagination['total_rows'] 	= $hasil;
	$pagination['per_page'] 	= "3";
	$pagination['url_segment']  = 4;
	$pagination['num_links']	= 2;
//Custom Paging Configuration
	$pagination['full_tag_open']	='<div class="pagination">';
	$pagination['full_tag_close']	='</div>';

	$pagination['first_link']		='First Page';
	$pagination['first_tag_open']	='<span class="firstlink">';
	$pagination['first_tag_close']	='</span>';

	$pagination['last_link']		='Last Page';
	$pagination['last_tag_open']	='<span class="lastlink">';
	$pagination['last_tag_close']	='</span>';

	$pagination['next_link']		='Next Page';
	$pagination['next_tag_open']	='<span class="nextlink">';
	$pagination['next_tag_close']	='</span>';

	$pagination['prev_link']		='Prev Page';
	$pagination['prev_tag_open']	='<span class="prevlink">';
	$pagination['prev_tag_close']	='</span>';

	$pagination['cur_tag_open']		='<span class="curlink">';
	$pagination['cur_tag_close']	='</span>';

	$pagination['num_tag_open']		='<span class="numlink">';
	$pagination['num_tag_close']	='</span>';

	$this->pagination->initialize($pagination);

	$hasil_pagination = $this->TampilDataPenjualanPagination($pagination['per_page'],
	$this->uri->segment(4), $data_pencarian);

	return $hasil_pagination;
	}

	public function notransaksi()
	{
	date_default_timezone_set('ASIA/JAKARTA');
	$this->db->select('MAX(no_transaksi) as no_transaksi');
	$query = $this->db->get($this->_table_header);
	$result = $query->row_array();

	$kode_transaksi_terakhir = $result[('no_transaksi')];

	$label = "TR";
	$no_urut_lama = (int) substr($kode_transaksi_terakhir,8,2);
	$no_urut_lama ++;

	$no_urut_baru = sprintf("%02s", $no_urut_lama);
	$tahun = (int) substr(date('y'), 1, 1);
	$bulan = date('m');
	$jam = date('H');
	if (($jam % 2) == 0) { $m = 'A';} else {$m = 'B';}
	$kode_transaksi_baru = $label . $tahun .$bulan .$jam .$m . $no_urut_baru;

	return $kode_transaksi_baru;
	}

	public function savePenjualanHeader()
	{
	$data['no_transaksi']	= $this->input->post('no_transaksi');
	$data['pembeli']	    = $this->input->post('nama_supplier');
	$data['tgl']			= date('Y-m-d');
	$data['flag']			= 1;
	$this->db->insert($this->_table_header, $data);
	}

	public function TampilDataPenjualanDetail($id)
	{
	 $query	= $this->db->query(
            "SELECT A.*, B.nama_barang FROM " . $this->_table_detail . " AS A INNER JOIN barang AS B ON A.kode_barang = B.kode_barang WHERE A.flag = 1 AND A.id_penjualan_h = '$id'"
        );
        return $query->result();
	}

	public function tampilBaru($tgl_awal,$tgl_akhir)
	{
		$this->db->select('penjualan_header.id_penjualan_h,penjualan_header.no_transaksi,penjualan_header.tgl,penjualan_header.pembeli,sum(penjualan_detail.qty) as qty,sum(penjualan_detail.jumlah) as jumlah,count(barang.stok) as stok,count(barang.kode_barang)');
		$this->db->from('penjualan_header');
		$this->db->join('penjualan_detail','penjualan_detail.id_penjualan_h=penjualan_header.id_penjualan_h');
		$this->db->join('barang','penjualan_detail.kode_barang=barang.kode_barang');
		$this->db->where("penjualan_header.tgl BETWEEN '$tgl_awal' AND '$tgl_akhir'");
		$this->db->group_by('penjualan_header.no_transaksi');

		$query=$this->db->get();
		return $query->result();
	}

	public function HasilPdf()
	{
		$this->db->select('penjualan_header.id_penjualan_h,penjualan_header.no_transaksi,penjualan_header.tgl,penjualan_header.pembeli,sum(penjualan_detail.qty) as qty,sum(penjualan_detail.jumlah) as jumlah,count(barang.stok) as stok,count(barang.kode_barang)');
		$this->db->from('penjualan_header');
		$this->db->join('penjualan_detail','penjualan_detail.id_penjualan_h=penjualan_header.id_penjualan_h');
		$this->db->join('barang','penjualan_detail.kode_barang=barang.kode_barang');
		//$this->db->where("penjualan_header.tgl BETWEEN '$tgl_awal' AND '$tgl_akhir'");
		$this->db->group_by('penjualan_header.no_transaksi');

		$query=$this->db->get();
		return $query->result();
	}

	public function savePenjualanDetail($id)
	{
		$qty    = $this->input->post('qty');
        $harga  = $this->input->post('harga_barang');
        $kode_barang = $this->input->post('kode_barang');
        $hargamenu = $this->Barang_model->cariHargaMenu($kode_barang);

        $data['id_penjualan_h'] = $id;
        $data['kode_barang']    = $kode_barang;
        $data['qty']            = $qty;
        $data['harga']          = $hargamenu;
        $data['jumlah']         = $qty * $hargamenu;
        $data['flag']           = 1;

	$this->db->insert($this->_table_detail, $data);
	}

	public function idTransaksiTerakhir()
	{
		$query = $this->db->query(
		"select * from " . $this->_table_header . " where flag = 1 order by id_penjualan_h desc limit 0,1"
		);
		$data_id = $query->result();
		foreach ($data_id as $data){
			$last_id = $data->id_penjualan_h;
			}
			return $last_id;
	}
}