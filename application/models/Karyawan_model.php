<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Karyawan_model extends CI_Model{
	private $_table = "karyawan";

	public function rules()
	{
		return[
		[
			'field' 		=> 'nik',
			'label' 		=> 'Nik',
			'rules' 		=> 'trim|required[max_length[10]|is_unique[karyawan.nik]',
			'errors'		=>[
			'required' 		=> 'Nik Tidak Boleh Kosong.',
			'max_length'	=> 'Nik Tidak Boleh lebih dari 10 karakter.',
			'is_unique'     => 'Nik Tidak Boleh Sama',
			],
		],
		[
			'field' 		=> 'nama_karyawan',
			'label' 		=> 'Nama Karyawan',
			'rules' 		=> 'required',
			'errors'		=> [
			'required' 		=> 'Nama Karyawan Tidak Boleh Kosong.',
			],
		],
		[
			'field' 		=> 'tempat_lahir',
			'label' 		=> 'Tempat Lahir',
			'rules' 		=> 'required',
			'errors'		=> [
			'required' 		=> 'Tempat Lahir Tidak Boleh Kosong.',
			],
		],
		[
			'field' 		=> 'tgl_lahir',
			'label' 		=> 'Tanggal Lahir',
			'rules' 		=> 'required',
			'errors'		=> [
			'required' 		=> 'Tanggal Tidak Boleh Kosong.',
			],
		[
			'field' 		=> 'jenis_kelamin',
			'label' 		=> 'Jenis Kelamin',
			'rules' 		=> 'required',
			'errors'		=> [
			'required' 		=> 'Jenis Kelamin Tidak Boleh Kosong.',
			],
		],
		[
			'field' 		=> 'alamat',
			'label' 		=> 'Alamat',
			'rules' 		=> 'required',
			'errors'		=> [
			'required' 		=> 'Alamat Tidak Boleh Kosong.',
			],
		],
		[
			'field' 		=> 'telp',
			'label' 		=> 'Telpon',
			'rules' 		=> 'required|numeric',
			'errors'		=> [
			'required' 		=> 'Telpon Tidak Boleh Kosong.',
			'numeric' 		=> 'Telpon Harus Angka.',
			],
		],
		[
			'field' 		=> 'kode_jabatan',
			'label' 		=> 'Kode Jabatan',
			'rules' 		=> 'required',
			'errors'		=> [
			'required' 		=> 'Kode Jabatan Tidak Boleh Kosong.',
			],
		],
		]
		];
	}

	public function rules1()
	{
		return[
		[
			'field' 		=> 'nik',
			'label' 		=> 'Nik',
			'rules' 		=> 'required[max_length[10]',
			'errors'		=>[
			'required' 		=> 'Nik Tidak Boleh Kosong.',
			'max_length'	=> 'Nik Tidak Boleh lebih dari 10 karakter.',
			],
		],
		[
			'field' 		=> 'nama_karyawan',
			'label' 		=> 'Nama Karyawan',
			'rules' 		=> 'required',
			'errors'		=> [
			'required' 		=> 'Nama Karyawan Tidak Boleh Kosong.',
			],
		],
		[
			'field' 		=> 'tempat_lahir',
			'label' 		=> 'Tempat Lahir',
			'rules' 		=> 'required',
			'errors'		=> [
			'required' 		=> 'Tempat Lahir Tidak Boleh Kosong.',
			],
		],
		[
			'field' 		=> 'tgl_lahir',
			'label' 		=> 'Tanggal Lahir',
			'rules' 		=> 'required',
			'errors'		=> [
			'required' 		=> 'Tanggal Tidak Boleh Kosong.',
			],
		[
			'field' 		=> 'jenis_kelamin',
			'label' 		=> 'Jenis Kelamin',
			'rules' 		=> 'required',
			'errors'		=> [
			'required' 		=> 'Jenis Kelamin Tidak Boleh Kosong.',
			],
		],
		[
			'field' 		=> 'alamat',
			'label' 		=> 'Alamat',
			'rules' 		=> 'required',
			'errors'		=> [
			'required' 		=> 'Alamat Tidak Boleh Kosong.',
			],
		],
		[
			'field' 		=> 'telp',
			'label' 		=> 'Telpon',
			'rules' 		=> 'required|numeric',
			'errors'		=> [
			'required' 		=> 'Telpon Tidak Boleh Kosong.',
			'numeric' 		=> 'Telpon Harus Angka.',
			],
		],
		[
			'field' 		=> 'kode_jabatan',
			'label' 		=> 'Kode Jabatan',
			'rules' 		=> 'required',
			'errors'		=> [
			'required' 		=> 'Kode Jabatan Tidak Boleh Kosong.',
			],
		],
		]
		];
	}

	public function TampilDataKaryawanPagination($perpage, $uri, $data_pencarian)
	{
	$this->db->select('*');
	if (!empty($data_pencarian)) {
		$this->db->like('nama_lengkap', $data_pencarian);
	}
	$this->db->order_by('nik','ASC');

	$get_data = $this->db->get($this->_table, $perpage, $uri);
	if ($get_data->num_rows() > 0) {
		return $get_data->result();
	} else {
		return null;
		}
	}
	public function tombolpagination($data_pencarian)
	{
	$this->db->like('nama_lengkap', $data_pencarian);
	$this->db->from($this->_table);
	$hasil = $this->db->count_all_results();

	$pagination['base_url'] 	= base_url().'karyawan/listkaryawan/load/';
	$pagination['total_rows'] 	= $hasil;
	$pagination['per_page'] 	= "3";
	$pagination['url_segment']  = 4;
	$pagination['num_links']	= 2;
//Custom Paging Configuration
	$pagination['full_tag_open']	='<div class="pagination">';
	$pagination['full_tag_close']	='</div>';

	$pagination['first_link']		='First Page';
	$pagination['first_tag_open']	='<span class="firstlink">';
	$pagination['first_tag_close']	='</span>';

	$pagination['last_link']		='Last Page';
	$pagination['last_tag_open']	='<span class="lastlink">';
	$pagination['last_tag_close']	='</span>';

	$pagination['next_link']		='Next Page';
	$pagination['next_tag_open']	='<span class="nextlink">';
	$pagination['next_tag_close']	='</span>';

	$pagination['prev_link']		='Prev Page';
	$pagination['prev_tag_open']	='<span class="prevlink">';
	$pagination['prev_tag_close']	='</span>';

	$pagination['cur_tag_open']		='<span class="curlink">';
	$pagination['cur_tag_close']	='</span>';

	$pagination['num_tag_open']		='<span class="numlink">';
	$pagination['num_tag_close']	='</span>';

	$this->pagination->initialize($pagination);

	$hasil_pagination = $this->TampilDataKaryawanPagination($pagination['per_page'],
	$this->uri->segment(4), $data_pencarian);

	return $hasil_pagination;
	}

	
	public function TampilDataKaryawan()
	{
	return $this->db->get($this->_table)->result();
	}
	
	public function TampilDataKaryawan2()
	{
	$query = $this->db->query("select * from karyawan WHERE flag = 1");
	return $query->result();
	}

	public function TampilDataKaryawan3()
	{
	$this->db->select('*');
	$this->db->order_by('nik','ASC');
	$result = $this->db->get($this->_table);
	return $result->result();
	}

	public function  save()
	{
	$nik_karyawan = $this->input->post('nik');
	$sql = $this->db->query("select nik from karyawan where nik ='$nik_karyawan'");
	$cek_nik = $sql->num_rows();
	if ($cek_nik > 0) 
	{
		$this->session->set_flashdata('nik');
		redirect('karyawan/InputKaryawan');
	} else {
		$nik_karyawan = $this->input->post('nik');
	}
	
	$data['nik']			= $nik_karyawan;
	$data['nama_lengkap']	= $this->input->post('nama_karyawan');
	$data['tempat_lahir']	= $this->input->post('tempat_lahir');
	$data['tgl_lahir']		= $this->input->post('tgl_lahir');
	$data['jenis_kelamin']	= $this->input->post('jenis_kelamin');
	$data['alamat']			= $this->input->post('alamat');
	$data['telp']			= $this->input->post('telp');
	$data['kode_jabatan']	= $this->input->post('kode_jabatan');
	$data['photo']			= $this->uploadPhoto($nik_karyawan);
	$data['flag']			= 1;
	$this->db->insert($this->_table, $data);
	}

	public function KodeUrut()
	{
		$this->db->select('MAX(nik) as nik');
		$query  = $this->db->get($this->_table);
		$result = $query->row_array();

		$nik_terakhir = $result['nik'];

		$thn = date('y');
		$bln = date('m');
		$no_urut_lama = (int) substr($nik_terakhir, 4, 3);
		$no_urut_lama ++;

		$no_urut_baru = sprintf("%03s", $no_urut_lama);
		$nik_baru	  = $thn . $bln . $no_urut_baru;

		return $nik_baru;
	}
	
	public function detail($nik)
	{
		$this->db->select('*');
		$this->db->where('nik', $nik);
		$this->db->where('flag', 1);
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	public function edit($nik)
	{
		$this->db->select('*');
		$this->db->where('nik', $nik);
		$this->db->where('flag', 1);
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	public function update($nik)
	{
	$data['nama_lengkap']	= $this->input->post('nama_karyawan');
	$data['tempat_lahir']	= $this->input->post('tempat_lahir');
	$data['tgl_lahir']		= $this->input->post('tgl_lahir');
	$data['jenis_kelamin']	= $this->input->post('jenis_kelamin');
	$data['alamat']			= $this->input->post('alamat');
	$data['telp']			= $this->input->post('telp');
	$data['kode_jabatan']	= $this->input->post('kode_jabatan');
	$data['flag']			= 1;
	
	if (!empty($_FILES["image"]["name"])) {
    $this->hapusPhoto($nik);
	$foto_karyawan = $this->uploadPhoto($nik);
	} else {
    $foto_karyawan = $this->input->post['old_image'];
	}
	$data['photo'] = $foto_karyawan;
	$this->db->where('nik', $nik);
	$this->db->update($this->_table, $data);
	}
	public function delete($nik)
	{
		$this->db->where('nik', $nik);
		$this->db->delete($this->_table);
	}
	private function uploadPhoto($nik)
	{
		$date_upload 			 = date('Ymd');
		$config['upload_path']   = './resources/foto';
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['file_name'] 	 = $date_upload .'-'. $nik;
		$config['overwrite'] 	 = true;
		$config['max_size'] 	 = 1024;

		$this->load->library('upload',$config);

		if ($this->upload->do_upload('image')) {
			$nama_file = $this->upload->data("file_name");
			} else {
				$nama_file = "default.png";
			}
		return $nama_file;
	}
	private function hapusPhoto($nik)
	{
		$data_karyawan = $this->detail($nik);
		foreach ($data_karyawan as $data) {
			$nama_file = $data->photo;
		}
		if ($nama_file != "default.png") {
			$path = "./resources/foto" . $nama_file;
			return unlink($path);
		}
	}
}