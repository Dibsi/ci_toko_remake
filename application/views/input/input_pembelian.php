<?=validation_errors();?>

 <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Input Pembelian</h3>
<form method="POST" action="<?=base_url()?>pembelian/InputPembelian">
  <form role="form">
              <div class="box-body">
                <div class="form-group">
                  <label for="no_transaksi">No Transaksi</label>
                  <input type="text" class="form-control" id="no_transaksi" name="no_transaksi" value="<?=$data_pembelian;?>" readonly>
                </div>
  <div class="form-group">
                  <label for="nama_supplier">Nama Supplier</label>
                  <select name="nama_supplier" class="form-control" id="nama_supplier">
                </div>
  <?php
  foreach ($data_supplier as $data){ ?>
	 <option value="<?= $data->kode_supplier; ?>">
     <?= $data->nama_supplier; ?>
     </option>
  <?php } ?>
    </select>
 </div>
   <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>