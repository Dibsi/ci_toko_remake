<?=validation_errors();?>

 <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Input Supplier</h3>
<form method="POST" action="<?=base_url()?>supplier/InputSupplier">
<table width="50%" border="0" cellpadding="5" bgcolor="#00CC66" align="center">
  <form role="form">
              <div class="box-body">
                <div class="form-group">
                  <label for="kode_supplier">Kode Supplier</label>
                  <input type="text" class="form-control" id="kode_supplier" name="kode_supplier" value="<?=$data_supplier;?>" readonly>
                </div>
                <div class="form-group">
                  <label for="nama_supplier">Nama Supplier</label>
                  <input type="text" class="form-control" id="nama_supplier" name="nama_supplier">
                </div>
    <div class="form-group">
                  <label for="telp">Telp</label>
                  <input type="text" class="form-control" id="telp" name="telp">
                </div>
  <div class="form-group">
                  <label for="alamat">Alamat</label>
                  <textarea class="form-control" rows="3" id="alamat" name="alamat">
                  </textarea>
                </div>
   <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>