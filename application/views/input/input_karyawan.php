
<?=validation_errors();?>

 <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Input Karyawan</h3>
<form method="POST" action="<?=base_url()?>karyawan/InputKaryawan" enctype="multipart/form-data">
<form role="form">
              <div class="box-body">
                <div class="form-group">
                  <label for="nik">Nik</label>
                  <input type="text" class="form-control" id="nik" name="nik" value="<?=$data_karyawan;?>" readonly >
                </div>
                <div class="form-group">
                  <label for="nama_karyawan">Nama Karyawan</label>
                  <input type="nama_karyawan" class="form-control" id="nama_karyawan" name="nama_karyawan">
                </div>

              
                <div class="form-group">
                  <label for="tempat_lahir">Tempat Lahir</label>
                  <input type="text" class="form-control" id="tempat_lahir" name="tempat_lahir">
                </div>
                <div class="form-group">
                  <label for="tgl_lahir">Tanggal Lahir</label>
                  <input type="text" class="form-control" id="tgl_lahir"  name="tgl_lahir" value="<?=set_value('tgl_lahir')?>"  autocomplete="off">
                </div>
 
                <div class="form-group">
                  <label for="jenis_kelamin">Jenis Kelamin</label>
                  <select name="jenis_kelamin" class="form-control" id="jenis_kelamin">
                  <option value="L"> Laki - Laki </option>
                  <option value="P"> Perempuan </option>
                </select>
                </div>

                <div class="form-group">
                  <label for="telp">Telp</label>
                  <input type="text" class="form-control" id="telp" name="telp">
                </div>
                <div class="form-group">
                  <label for="alamat">Alamat</label>
                  <textarea class="form-control" rows="3" id="alamat" name="alamat">
                  </textarea>
                </div>

                <div class="form-group">
                  <label for="alamat">Jabatan</label>
                  <select name="kode_jabatan" class="form-control" id="kode_jabatan">
                </div>
  <?php
  foreach ($data_jabatan as $data){ ?>
	 <option value="<?= $data->kode_jabatan; ?>">
     <?= $data->nama_jabatan; ?></option>
  <?php } ?>
    </select>
</div>
    <div class="form-group">
                  <label for="exampleInputFile">File input</label>
                  <input class="form-control" type="file" id="exampleInputFile" name="image">
                </div>
   <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>