<?=validation_errors();?>

 <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Input Penjualan Detail</h3>
<form method="POST" action="<?=base_url()?>penjualan/InputPenjualanDetail/<?= $id_header; ?>">
<div class="form-group">
                  <label for="kode_barang">Nama Barang</label>
                  <select name="kode_barang" class="form-control" id="kode_barang">
                </div>
  <?php
  foreach ($data_barang as $data){ ?>
   <option value="<?= $data->kode_barang; ?>">
     <?= $data->nama_barang; ?>
     </option>
  <?php } ?>
    </select>
  <div class="form-group">
                  <label for="qty">Qty</label>
                  <input type="text" class="form-control" id="qty" name="qty">
                </div>
  <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
  <td>&nbsp;</td>
  <td>&nbsp;</td>

 <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <th>No</th>
                  <th>Kode Barang</th>
                  <th>Nama Barang</th>
                  <th>Qty</th>
                  <th>Harga</th>
                  <th>Jumlah</th>
                </tr>
    <?php
   $no = 0;
  $total_hitung = 0;
  foreach ($data_penjualan_detail as $data){
  $no++;
  ?>
  <tr bgcolor="#CCCCCC">
     <td><?= $no;?></td>
    <td><?= $data->kode_barang; ?></td>
    <td><?= $data->nama_barang; ?></td>
    <td><?= $data->qty; ?></td>
    <td align="right">Rp. <?= number_format($data->harga); ?> ,-</td>
    <td  align="right">Rp. <?= number_format($data->jumlah); ?> ,-</td>
   </tr>
  <?php
   $total_hitung += $data->jumlah;
   } ?>
    <tr align="center">
    <td colspan="4" align="right"><b>TOTAL</b></td>
  <td  align="right">Rp. <b><?= number_format($total_hitung); ?></b></td>
   
    </tr>

</form>