<head>
  <center><h1>Report Penjualan</h1></center>
  <form method="POST" action="<?=base_url()?>penjualan/ListPenjualan">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#tgl_awal" ).datepicker({dateformat: "yy-mm-dd"});
    $( "#tgl_akhir" ).datepicker({dateformat: "yy-mm-dd"});
  } );
  </script>
  <script>
  $(document).ready(function(){

    $('#proses').on('click', function(event){
      event.preventDefault();
      var tgl_awal = $('#tgl_awal').val();
      var tgl_akhir= $('$tgl_akhir').val();

      if (tgl_awal == '' || tgl_akhir == '') {
          alert('Tanggal Tidak Boleh Kosong');
          } else if (new Date(tgl_awal) > new Date(tgl_akhir)) {
            alert('Format Waktu Salah Input');
          } else {
            $('#forms').submit();
          }  
    });
    });
</script>
</head>
<body>
 <div class="box-body">
              <!-- Date -->
              <div class="form-group">
                <label>Tanggal Awal :</label>

                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" name="tgl_awal" id="tgl_awal">
                </div>
                <!-- /.input group -->
              </div>
              <!-- Date -->
              <div class="form-group">
                <label>Tanggal Akhir:</label>

                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" name="tgl_akhir" id="tgl_akhir">
              </div>
                </div>
<div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>    
            </div>
          </div>
</body>
