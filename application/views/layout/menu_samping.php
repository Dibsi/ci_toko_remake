  <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-edit"></i>
            <span>Master</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url();?>barang/listbarang"><i class="fa fa-circle-o"></i> Barang</a></li>
            <li><a href="<?=base_url();?>jabatan/listjabatan"><i class="fa fa-circle-o"></i>  Jabatan</a></li>
            <li><a href="<?=base_url();?>jenis/listjenis"><i class="fa fa-circle-o"></i>  Jenis</a></li>
            <li><a href="<?=base_url();?>karyawan/listkaryawan"><i class="fa fa-circle-o"></i>  Karyawan</a></li>
            <li><a href="<?=base_url();?>pembelian/listpembelian1"><i class="fa fa-circle-o"></i>  Pembelian</a></li>
            <li><a href="<?=base_url();?>penjualan/listpenjualan1"><i class="fa fa-circle-o"></i>  Penjualan</a></li>
            <li><a href="<?=base_url();?>supplier/listsupplier"><i class="fa fa-circle-o"></i>  Supplier</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Transaksi</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
          <li><a href="<?=base_url();?>pembelian/InputPembelian"><i class="fa fa-circle-o"></i> Pembelian</a></li>
          <li><a href="<?=base_url();?>penjualan/InputPenjualan"><i class="fa fa-circle-o"></i> penjualan</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-share"></i> <span>Reporting</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li>
              <a href="<?=base_url();?>pembelian/InputPembelian1"><i class="fa fa-circle-o"></i> Pembelian
              </a>
                <a href="<?=base_url();?>penjualan/InputPenjualan1"><i class="fa fa-circle-o"></i> Penjualan
                  </a>
            </li>

          </ul>
        </li>
      </ul>