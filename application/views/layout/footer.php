<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2019 Adib Bisyri Musyaffa</a>.</strong> All rights
    reserved.
  </footer>