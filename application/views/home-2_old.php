<?php
  defined('BASEPATH') OR exit('No direct script access allowed');
$user_login = $this->session->userdata();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Toko Jaya Abadi</title>
</head>
<body>
<?php
if ($user_login['tipe'] == "1"){
  $this->load->view('template/view_menu');
} else {
  $this->load->view('template/view_user');
}
?>
<table width="1350" height="30" border="0">
  
</table>
<tr>
  </tr>
<?php $this->load->view($content); ?>
<div style="background:#333;color:#fff;margin:5px 0;padding:10px 0" align="center" class='copyright'>Copyright © 2019. Adib Bisyri Musyaffa </div>
<p align="center">&nbsp;</p>
</body>
</html>