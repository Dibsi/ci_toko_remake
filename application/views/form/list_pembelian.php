<center><H1>Data Report Pembelian</H1></center>
<center><h1><p>Dari Tanggal
<?php
    $pisah=explode('/', $tgl_awal);
    $array=array($pisah[1],$pisah[0],$pisah[2]);
    $tgl_awal=implode('-', $array);

    $pisah=explode('/', $tgl_akhir);
    $array=array($pisah[1],$pisah[0],$pisah[2]);
    $tgl_akhir=implode('-', $array);
 echo $tgl_awal; ?>
 s/d
 <?php echo $tgl_akhir; ?>
</p>
</h1>
</center>
<td width="150"><a href="<?=base_url();?>pembelian/CetakPdf/<?=$tgl_awal;?>/<?=$tgl_akhir;?>"><input type="submit" name="submit" value="Cetak PDF"></td>
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <div class="box-tools">
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <th>No</th>
                  <th>Id Pembelian</th>
                  <th>No Transaksi</th>
                  <th>Tgl</th>
                  <th>Total Barang</th>
                  <th>Qty</th>
                  <th>Jumlah Pembelian</th>
                </tr>
    <?php
   $no = 0;
  $total_hitung = 0;
  foreach ($data_pembelian_detail as $data){
  $no++;
  ?>
  <tr bgcolor="#CCCCCC">
    <td><?= $no;?></td>
    <td><?= $data->id_pembelian_h; ?></td>
    <td><?= $data->no_transaksi; ?></td>
    <td><?= $data->tgl; ?></td>
    <td><?= $data->stok; ?></td>
    <td><?= $data->qty; ?></td>
    <td align="right">Rp. <?= number_format($data->jumlah); ?> ,-</td>
   </tr>
  <?php
  $total_hitung += $data->jumlah;
   } ?>
    <tr align="center">
    <td colspan="4" align="right"><b>TOTAL KESELURUHAN</b></td>
  <td  align="right">Rp. <b><?= number_format($total_hitung); ?></b></td>
   
    </tr>
           </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>