<form action="<?=base_url()?>jenis/listjenis" method="POST">    
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
            <center>
              <h3 class="box-title">Data Jenis</h3>
              </center>
              <div align="left"><a href="<?=base_url()?>jenis/InputJenis">[Input Jenis]</a></div>
              <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="caridata" id="caridata" autocomplete="off" placeholder="Cari Nama" />
                  <div class="input-group-btn">
                    <button type="submit" name="tombol_cari" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <th>No</th>
                  <th>Kode Jenis</th>
                  <th>Nama Jenis</th>
                  <th>Aksi</th>
                </tr>
                 <?php
    $data_posisi = $this->uri->segment(4);
    $no = $data_posisi;
    if (count($data_jenis) > 0 ) {
    foreach ($data_jenis as $data){
    $no++;
    ?>
                <tr>
                    <td><?=$no;?></td>
                    <td><?= $data->kode_jenis; ?></td>
                    <td><?= $data->nama_jenis; ?></td>
  <td>
       <span class="badge bg-yellow"><a href="<?=base_url();?>jenis/detailjenis/<?= $data->kode_jenis; ?>">Detail </a></span>
      |<span class="badge bg-blue"><a href="<?=base_url();?>jenis/editjenis/<?= $data->kode_jenis; ?>"/> Edit</a></span>
      |<span class="badge bg-red"><a href="<?=base_url();?>jenis/deletejenis/<?= $data->kode_jenis; ?>" onclick="return confirm('Yakin Ingin Hapus Data?');"/> Delete</a></span>
    </td>
  <?php
   }
   ?>
   
           </table>
           <div class="box-footer clearfix">
              <ul class="pagination pagination-sm no-margin pull-left">
                <li>&laquo;</li>
                <li><b> Halaman :</b> <?= $this->pagination->create_links();?></li>
                <li>&raquo;</li>
              </ul>
            </div>
<?php
  } else {
?>
<tr align="center">
  <td colspan="7">--- Tidak Ada Data ---</td>
</tr>
<?php
  }
?>   
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
</form>