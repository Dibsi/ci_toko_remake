
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
               <center>
              <h3 class="box-title">Data Penjualan</h3>
              </center>
              <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="caridata" id="caridata" autocomplete="off" placeholder="Cari Nama" />
                  <div class="input-group-btn">
                    <button type="submit" name="tombol_cari" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <br>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <th>No</th>
                  <th>No Transaksi</th>
                  <th>Tgl Transaksi</th>
                  <th>Nama Pembeli</th>
                  <th>Aksi</th>
                </tr>
    <?php
   $data_posisi = $this->uri->segment(4);
    $no = $data_posisi;
    if (count($data_penjualan) > 0 ) {
  foreach ($data_penjualan as $data){
  $no++;
  ?>
  <tr>
    <td><?= $no;?></td>
    <td><?= $data->no_transaksi; ?></td>
    <td><?= $data->tgl; ?></td>
    <td><?= $data->pembeli; ?></td>
     <td>
      <span class="badge bg-yellow">Detail </span>
  <?php
   }
   ?>


           </table>
              <div class="box-footer clearfix">
              <ul class="pagination pagination-sm no-margin pull-left">
                <li>&laquo;</li>
                <li><b> Halaman : </b> <?= $this->pagination->create_links();?></li>
                <li>&raquo;</li>
              </ul>
            </div>
<?php
  } else {
?>
<tr align="center">
  <td colspan="7">--- Tidak Ada Data ---</td>
</tr>
<?php
  }
?>  
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>