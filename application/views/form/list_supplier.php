<form action="<?=base_url()?>supplier/listsupplier" method="POST">    
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
            <center>
              <h3 class="box-title">Data Supplier</h3>
              </center>
              <div align="left"><a href="<?=base_url()?>supplier/InputSupplier">[Input Supplier]</a></div>
              <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="caridata" id="caridata" autocomplete="off" placeholder="Cari Nama" />
                  <div class="input-group-btn">
                    <button type="submit" name="tombol_cari" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <th>No</th>
                  <th>Kode Supplier</th>
                  <th>Nama Supplier</th>
                  <th>Alamat</th>
                  <th>Telp</th>
                  <th>Aksi</th>
                </tr>
                 <?php
    $data_posisi = $this->uri->segment(4);
    $no = $data_posisi;
    if (count($data_supplier) > 0 ) {
    foreach ($data_supplier as $data){
    $no++;
    ?>
                <tr>
                    <td><?=$no;?></td>
                    <td><?= $data->kode_supplier; ?></td>
                    <td><?= $data->nama_supplier; ?></td>
                    <td><?= $data->alamat; ?></td>
                    <td><?= $data->telp; ?></td>
  <td>
        <span class="badge bg-yellow"><a href="<?=base_url();?>supplier/detailsupplier/<?= $data->kode_supplier; ?>">Detail </a></span>
      |<span class="badge bg-blue"><a href="<?=base_url();?>supplier/editsupplier/<?= $data->kode_supplier; ?>"/> Edit  </a></span>
      |<span class="badge bg-red"><a href="<?=base_url();?>supplier/deletesupplier/<?= $data->kode_supplier; ?>" onclick="return confirm('Yakin Ingin Hapus Data?');"/> Delete</a></span>
    </td>
  </tr>
  </tr>
  <?php
   }
   ?>


           </table>
              <div class="box-footer clearfix">
              <ul class="pagination pagination-sm no-margin pull-left">
                <li>&laquo;</li>
                <li><b> Halaman : </b> <?= $this->pagination->create_links();?></li>
                <li>&raquo;</li>
              </ul>
            </div>
<?php
  } else {
?>
<tr align="center">
  <td colspan="7">--- Tidak Ada Data ---</td>
</tr>
<?php
  }
?>  
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
</form>