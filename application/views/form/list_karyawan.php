
<form action="<?=base_url()?>karyawan/listkaryawan" method="POST">      
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
            <center>
              <h3 class="box-title">Data Karyawan</h3>
              </center>
              <div align="left"><a href="<?=base_url()?>karyawan/InputKaryawan">[Input Karyawan]</a></div>
              <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="caridata" id="caridata" autocomplete="off" placeholder="Cari Nama" />
                  <div class="input-group-btn">
                    <button type="submit" name="tombol_cari" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <th>No</th>
                  <th>Nik</th>
                  <th>Nama</th>
                  <th>Tempat Lahir</th>
                  <th>Telp</th>
                  <th>Foto</th>
                  <th>Aksi</th>
                </tr>
                 <?php
    $data_posisi = $this->uri->segment(4);
    $no = $data_posisi;
    if (count($data_karyawan) > 0 ) {
    foreach ($data_karyawan as $data){
    $no++;
    ?>
                <tr>
                  <td><?=$no;?></td>
                  <td><?= $data->nik; ?></td>
                  <td><?= $data->nama_lengkap; ?></td>
                   <td><?= $data->tempat_lahir; ?></td>
                 <td><?= $data->telp; ?></td>
                 <td>
                 <?php
    if (!empty($data->photo)) {
    ?>
   <img src="<?=base_url();?>./resources/foto/<?=$data->photo;?>" width='80' height='70'/>
    <?php
  }else{
    ?>
  <img src="<?=base_url();?>./resources/foto/default.png" width='80' height='70'/>
    <?php
  }
  ?>
  </td>

  <td>
    <span class="badge bg-yellow"><a href="<?=base_url();?>karyawan/detailkaryawan/<?= $data->nik; ?>">Detail </a></span>
     | <span class="badge bg-blue"><a href="<?=base_url();?>karyawan/editkaryawan/<?= $data->nik; ?>"/> Edit  </a></span>
     | <span class="badge bg-red"><a href="<?=base_url();?>karyawan/deletekaryawan/<?= $data->nik; ?>" onclick="return confirm('Yakin Ingin Hapus Data?');"/>Delete</a></span>
     </td>
  <?php
   }
   ?>


           </table>
              <div class="box-footer clearfix">
              <ul class="pagination pagination-sm no-margin pull-left">
                <li>&laquo;</li>
                <li><b> Halaman : </b> <?= $this->pagination->create_links();?></li>
                <li>&raquo;</li>
              </ul>
            </div>
<?php
  } else {
?>
<tr align="center">
  <td colspan="7">--- Tidak Ada Data ---</td>
</tr>
<?php
  }
?>  
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
</form>