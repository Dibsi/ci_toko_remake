<?php
	foreach ($detail_barang as $data) {
		$kode_barang	= $data->kode_barang;
		$nama_barang	= $data->nama_barang;
		$harga_barang	= $data->harga_barang;
		$kode_jenis		= $data->kode_jenis;
	}
?>
<?=validation_errors();?>
<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Barang</h3>
<form method="POST" action="<?=base_url()?>barang/EditBarang/<?= $kode_barang; ?>">
<form role="form">
              <div class="box-body">
                <div class="form-group">
                  <label for="kode_barang">Kode Barang</label>
                  <input type="text" class="form-control" id="kode_barang" name="kode_barang" value="<?=$kode_barang;?>" readonly >
                </div>
                <div class="form-group">
                  <label for="nama_barang">Nama Barang</label>
                  <input type="nama_barang" class="form-control" id="nama_barang" name="nama_barang" value="<?=$nama_barang;?>">
                </div>
    <div class="form-group">
                  <label for="harga_barang">Harga Barang</label>
                  <input type="harga_barang" class="form-control" id="harga_barang" name="harga_barang" value="<?=$harga_barang;?>">
                </div>
    <div class="form-group">
                  <label for="alamat">Jenis Barang</label>
                  <select name="kode_jenis" class="form-control" id="kode_jenis">
                </div>
  <?php
  foreach ($data_jenis as $data){ 
  	 $select_jenis_barang = ($data->kode_jenis ==
	 $kode_jenis) ? 'selected' : '';
	 ?>
	 <option value="<?= $data->kode_jenis; ?>">
     <?= $data->kode_jenis; ?> |
      <?= $data->nama_jenis; ?> 
     </option>
  <?php } ?>
    </select>
 <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>