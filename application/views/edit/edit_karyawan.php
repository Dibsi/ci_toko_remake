<?php
	foreach ($detail_karyawan as $data) {
		$nik			      = $data->nik;
		$nama_lengkap	  = $data->nama_lengkap;
		$tempat_lahir	  = $data->tempat_lahir;
		$tgl_lahir		  = $data->tgl_lahir;
		$jenis_kelamin  = $data->jenis_kelamin;
		$alamat			    = $data->alamat;
		$telp			      = $data->telp;
		$kode_jabatan	  = $data->kode_jabatan;
    $photo          = $data->photo;
	}
?>
<?=validation_errors();?>
<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Karyawan</h3>
<form method="POST" action="<?=base_url()?>karyawan/EditKaryawan/<?= $nik; ?>" enctype="multipart/form-data">
<form role="form">
              <div class="box-body">
                <div class="form-group">
                  <label for="nik">Nik</label>
                  <input type="text" class="form-control" id="nik" name="nik" value="<?=$nik;?>" readonly >
                </div>
                <div class="form-group">
                  <label for="nama_karyawan">Nama Karyawan</label>
                  <input type="nama_karyawan" class="form-control" id="nama_karyawan" name="nama_karyawan" value="<?=$nama_lengkap?>">
                </div>
  <div class="form-group">
                  <label for="tempat_lahir">Tempat Lahir</label>
                  <input type="text" class="form-control" id="tempat_lahir" name="tempat_lahir" value="<?=$tempat_lahir?>">
                </div>
 <div class="form-group">
                  <label for="jenis_kelamin">Jenis Kelamin</label>
    <?php
    	if ($jenis_kelamin == 'L'){
			$slc_p = '';
			$slc_l = 'selected';
		}elseif ($jenis_kelamin == 'P'){
			$slc_l = '';
			$slc_p = 'selected';
		}else{
			$slc_p = '';
			$slc_l = '';	
		}
	?>	
     <select name="jenis_kelamin" class="form-control" id="jenis_kelamin">
    	<option <?=$slc_l;?> value="L">Laki-laki</option>
        <option <?=$slc_p;?> value="P">Perempuan</option>
    </select>
    </div>
  <div class="form-group">
                  <label for="tgl_lahir">Tanggal Lahir</label>
                  <input type="text" class="form-control" id="tgl_lahir" name="tgl_lahir" value="<?=$tgl_lahir?>" autocomplete="off">
                </div>
  <div class="form-group">
                  <label for="telp">Telp</label>
                  <input type="text" class="form-control" id="telp" name="telp" value="<?=$telp;?>">
                </div>
  <div class="form-group">
                  <label for="alamat">Alamat</label>
    <textarea class="form-control" value=<?= $alamat; ?> name="alamat" id="alamat"cols="45" rows="3"><?=$alamat;?></textarea>
 </div>
 <div class="form-group">
                  <label for="kode_jabatan">Jabatan</label>
                  <select name="kode_jabatan" class="form-control" id="kode_jabatan">
                </div>
  <?php
  foreach ($data_jabatan as $data){ 
   $select_jabatan = ($data->kode_jabatan ==
	 $kode_jabatan) ? 'selected' : '';
	 ?>
	 <option value="<?= $data->kode_jabatan; ?>">
     <?=$data->nama_jabatan;?></option>
  <?php } ?>
    </select>
  </div>
    <input type="hidden" id="exampleInputFile1" name="old_image" value="<?= $photo; ?>">
    <div class="form-group">
                  <label for="exampleInputFile">File input</label>
                  <input type="file" id="exampleInputFile" name="image">
                </div>
    <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>