<?php
	foreach ($detail_supplier as $data) {
		$kode_supplier	= $data->kode_supplier;
		$nama_supplier	= $data->nama_supplier;
		$alamat			= $data->alamat;
		$telp			= $data->telp;
	}
?>
<?=validation_errors();?>
<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Supplier</h3>
<form method="POST" action="<?=base_url()?>supplier/EditSupplier/<?= $kode_supplier; ?>">
<div class="box-body">
                <div class="form-group">
                  <label for="kode_supplier">Kode Supplier</label>
                  <input type="text" class="form-control" id="kode_supplier" name="kode_supplier" value="<?=$kode_supplier;?>" readonly>
                </div>
                <div class="form-group">
                  <label for="nama_supplier">Nama Supplier</label>
                  <input type="nama_supplier" class="form-control" id="nama_supplier" name="nama_supplier" value="<?=$nama_supplier?>">
                </div>
    <div class="form-group">
                  <label for="telp">Telp</label>
                  <input type="telp" class="form-control" id="telp" name="telp" value="<?=$telp?>">
                </div>
  <div class="form-group">
                  <label for="alamat">Alamat</label>
    <textarea class="form-control" value=<?= $alamat; ?> name="alamat" id="alamat"cols="45" rows="3"><?=$alamat;?></textarea>
   <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>