<?php
  foreach ($detail_jabatan as $data) {
    $kode_jabatan = $data->kode_jabatan;
    $nama_jabatan = $data->nama_jabatan;
    $keterangan   = $data->keterangan;
  }
?>
<?=validation_errors();?>
<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Jabatan</h3>
<form method="POST" action="<?=base_url()?>jabatan/EditJabatan/<?= $kode_jabatan; ?>">
<form role="form">
              <div class="box-body">
                <div class="form-group">
                  <label for="kode_jabatan">Kode Jabatan</label>
                  <input type="text" class="form-control" id="kode_jabatan" name="kode_jabatan" value="<?=$kode_jabatan;?>" readonly>
                </div>
                <div class="form-group">
                  <label for="nama_jabatan">Nama Jabatan</label>
                  <input type="nama_jabatan" class="form-control" id="nama_jabatan" name="nama_jabatan" value="<?=$nama_jabatan?>">
                </div>
    <div class="form-group">
                  <label for="keterangan">Keterangan</label>
                  <input type="keterangan" class="form-control" id="keterangan" name="keterangan" value="<?=$keterangan?>">
                </div>
  <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>